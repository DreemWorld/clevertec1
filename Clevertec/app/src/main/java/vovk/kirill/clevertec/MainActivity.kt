package vovk.kirill.clevertec

import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private var helloWorld: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        helloWorld = findViewById<TextView>(R.id.hello_world)
        if (Build.VERSION.SDK_INT > 24)
        {
                helloWorld!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.blue))
        }
    }
}